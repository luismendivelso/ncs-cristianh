
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import play.data._
import play.core.j.PlayFormsMagicForJava._

object index extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[List[models.Song],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(songs: List[models.Song]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {

def /*3.2*/display/*3.9*/(song: models.Song):play.twirl.api.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*3.30*/("""
"""),format.raw/*4.1*/("""<div class="container">
    <div class="card grey lighten-2" align="center">
        <strong>"""),_display_(/*6.18*/song/*6.22*/.getArtist().getUsername()),format.raw/*6.48*/(""" """),format.raw/*6.49*/("""- """),_display_(/*6.52*/song/*6.56*/.getTitle()),format.raw/*6.67*/("""</strong>
        <br>
        <div class="section"/>
        <div class="row">
            <audio id=""""),_display_(/*10.25*/song/*10.29*/.getUri()),format.raw/*10.38*/("""" src=""""),_display_(/*10.46*/song/*10.50*/.getUri()),format.raw/*10.59*/("""" controls>
                Your browser does not support audio.
            </audio>
        </div>
    </div>
</div>


</div>
""")))};
Seq[Any](format.raw/*1.28*/("""

"""),format.raw/*19.2*/("""

"""),_display_(/*21.2*/main("Home")/*21.14*/ {_display_(Seq[Any](format.raw/*21.16*/("""
    """),_display_(/*22.6*/if(songs.isEmpty())/*22.25*/{_display_(Seq[Any](format.raw/*22.26*/("""
        """),format.raw/*23.9*/("""<div class="section"/>
        <div class="container">
            <strong>No Existen ni Canciones ni Artistas Registrados Por ahora... Registrate y Sube Canciones sin Copyright...</strong>
            <br>
            <strong>don't exist any Song and Artist the platform, sign in and update songs without Copyright...</strong>
        </div>
    """)))}/*29.6*/else/*29.10*/{_display_(Seq[Any](format.raw/*29.11*/("""
        """),_display_(/*30.10*/for(son <- songs) yield /*30.27*/{_display_(Seq[Any](format.raw/*30.28*/("""
            """),_display_(/*31.14*/display(son)),format.raw/*31.26*/("""
        """)))}),format.raw/*32.10*/("""
    """)))}),format.raw/*33.6*/("""
""")))}),format.raw/*34.2*/("""
"""))
      }
    }
  }

  def render(songs:List[models.Song]): play.twirl.api.HtmlFormat.Appendable = apply(songs)

  def f:((List[models.Song]) => play.twirl.api.HtmlFormat.Appendable) = (songs) => apply(songs)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: Mon Jul 17 10:26:52 COT 2017
                  SOURCE: C:/Users/Asus/Documents/Cristian/Tesis/ncsong/app/views/index.scala.html
                  HASH: 76c9c0a45c8af07e94264a41367df2e25a4b4917
                  MATRIX: 959->1|1063->32|1077->39|1176->60|1204->62|1326->158|1338->162|1384->188|1412->189|1441->192|1453->196|1484->207|1619->315|1632->319|1662->328|1697->336|1710->340|1740->349|1917->27|1948->487|1979->492|2000->504|2040->506|2073->513|2101->532|2140->533|2177->543|2549->897|2562->901|2601->902|2639->913|2672->930|2711->931|2753->946|2786->958|2828->969|2865->976|2898->979
                  LINES: 28->1|32->3|32->3|34->3|35->4|37->6|37->6|37->6|37->6|37->6|37->6|37->6|41->10|41->10|41->10|41->10|41->10|41->10|51->1|53->19|55->21|55->21|55->21|56->22|56->22|56->22|57->23|63->29|63->29|63->29|64->30|64->30|64->30|65->31|65->31|66->32|67->33|68->34
                  -- GENERATED --
              */
          