
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Asus/Documents/Cristian/Tesis/ncsong/conf/routes
// @DATE:Mon Jul 17 10:11:14 COT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
