
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Asus/Documents/Cristian/Tesis/ncsong/conf/routes
// @DATE:Mon Jul 17 10:11:14 COT 2017

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseSongController SongController = new controllers.ReverseSongController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseArtistController ArtistController = new controllers.ReverseArtistController(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseSongController SongController = new controllers.javascript.ReverseSongController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseArtistController ArtistController = new controllers.javascript.ReverseArtistController(RoutesPrefix.byNamePrefix());
  }

}
