
// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Asus/Documents/Cristian/Tesis/ncsong/conf/routes
// @DATE:Mon Jul 17 10:11:14 COT 2017

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:9
  class ReverseSongController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:34
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SongController.delete",
      """
        function(uri0,username1) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "logs/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username1)) + "/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("uri", encodeURIComponent(uri0)) + "/d"})
        }
      """
    )
  
    // @LINE:9
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SongController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:31
    def upload: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.SongController.upload",
      """
        function(username0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "logs/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username0)) + "/uploadSong"})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }

  // @LINE:12
  class ReverseArtistController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:16
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.show",
      """
        function(username0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logs/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username0))})
        }
      """
    )
  
    // @LINE:13
    def addArtist: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.addArtist",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "signin"})
        }
      """
    )
  
    // @LINE:30
    def uploadSong: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.uploadSong",
      """
        function(username0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logs/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username0)) + "/uploadSong"})
        }
      """
    )
  
    // @LINE:22
    def edit: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.edit",
      """
        function(username0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logs/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username0)) + "/edit"})
        }
      """
    )
  
    // @LINE:12
    def create: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.create",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "signin"})
        }
      """
    )
  
    // @LINE:19
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.delete",
      """
        function(username0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "logs/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username0))})
        }
      """
    )
  
    // @LINE:26
    def search: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.search",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "log"})
        }
      """
    )
  
    // @LINE:23
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.update",
      """
        function(username0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "logs/" + (""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username0)) + "/edit"})
        }
      """
    )
  
    // @LINE:27
    def buscar: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.ArtistController.buscar",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "log"})
        }
      """
    )
  
  }


}
