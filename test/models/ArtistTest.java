package models;

import static org.junit.Assert.*;
import org.junit.Test;
import models.*;

public class ArtistTest{

    @Test
    public void testGets(){
        Artist art = new Artist("username", "password", "name", "lastname");
        assertEquals("El usuername debera ser : username",art.getUsername(), "username");
        assertEquals("El password deberia ser : password",art.getPass(),"password");
        assertEquals("El name deberia ser : name",art.getName(), "name");
        assertEquals("El lastname deberia ser : lastname", art.getLastname(), "lastname");
    }

    @Test
    public void testSets(){
        Artist art = new Artist();
        art.setUsername("username");
        art.setPass("password");
        art.setName("name");
        art.setLastname("lastname");
        assertEquals("El usuername debera ser : username",art.getUsername(), "username");
        assertEquals("El password deberia ser : password",art.getPass(),"password");
        assertEquals("El name deberia ser : name",art.getName(), "name");
        assertEquals("El lastname deberia ser : lastname", art.getLastname(), "lastname");
    }
}